import (
	"fmt";
	"sort"
)

// sorted array used to store the left position
type OrderedArray struct {
	arr []int
}

func (oa *OrderedArray) Init(arr []int) *OrderedArray {
	oa.arr = arr
	if len(arr) > 1 {
		sort.Slice(arr, func(i, j int) bool { return arr[i]<arr[j] })
	}
	return oa
}

func (oa *OrderedArray) Get(index int) int {
	return oa.arr[index]
}

// binary search to find the lower bound for the given item
func (oa *OrderedArray) LowerBound(item int) int {
	i, j := 0, len(oa.arr)
	for i < j {
		h := i + (j-i)>>1
		if oa.arr[h]< item {
			i = h + 1
		} else {
			j = h
		}
	}
	return i
}

// add item into the sorted array
func (oa *OrderedArray) addItem(item int) {
	i := oa.LowerBound(item)
	if i != len(oa.arr) {
		oa.arr = append(oa.arr, 0)
		copy(oa.arr[i+1:], oa.arr[i:])
		oa.arr[i] = item
	} else {
		oa.arr = append(oa.arr, item)
	}
}

//remove item from the sorted array
func (oa *OrderedArray) removeItem(left, right int) {
	if right != len(oa.arr) {
		copy(oa.arr[left:], oa.arr[right:])
	}
	oa.arr = oa.arr[:len(oa.arr)-(right-left)]
}

//range list struct
// m stores left&right mapping
// oa stores sorted left array
type RangeList struct {
	//map[left]=right
	m  map[int]int
	//left
	oa OrderedArray
}

//RangeList Constructor
func Constructor() RangeList {
	this := RangeList{m: map[int]int{}}
	this.oa.Init(nil)
	return this
}

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a <= b {
		return b
	}
	return a
}

//find the new left/right base on the given left/right
func (rangeList *RangeList) find(left, right int) (int, int) {
	//find the lower bound for both left and right
	l, r := rangeList.oa.LowerBound(left+1), rangeList.oa.LowerBound(right+1)
	if l != 0 {
		l--
		if rangeList.m[rangeList.oa.Get(l)] < left {
			l++
		}
	}
	//1==r means no intersection
	if l == r {
		return left, right
	}
	//if there's intersection, find the new left/right
	left = min(left, rangeList.oa.Get(l))
	right = max(right, rangeList.m[rangeList.oa.Get(r-1)])
	//remove the contained part
	for i := l; i < r; i++ {
		delete(rangeList.m, rangeList.oa.Get(i))
	}
	rangeList.oa.removeItem(l, r)
	return left, right
}

//add range based on the given left and right
func (rangeList *RangeList) AddRange(left int, right int) {
	l, r := rangeList.find(left, right)
	if _, ok := rangeList.m[l]; !ok {
		rangeList.oa.addItem(l)
	}
	rangeList.m[l] = r
}

//remove range based on the given left and right
func (rangeList *RangeList) RemoveRange(left int, right int) {
	l, r := rangeList.find(left, right)
	if left > l {
		if _, ok := rangeList.m[l]; !ok {
			rangeList.oa.addItem(l)
		}
		rangeList.m[l] = left
	}
	if right < r {
		if _, ok := rangeList.m[right]; !ok {
			rangeList.oa.addItem(right)
		}
		rangeList.m[right] = r
	}
}

func (rangeList *RangeList) Print() {
	for i := 0; i < len(rangeList.oa.arr); i++ {
		left := rangeList.oa.Get(i)
		fmt.Print("[")
		fmt.Print(left)
		fmt.Print(", ")
		fmt.Print(rangeList.m[left])
		fmt.Print(") ")
	}
	fmt.Println()
}

func main(){
	rl := Constructor()
	rl.AddRange(1, 5)
	rl.Print()
	rl.AddRange(10, 20)
	rl.Print()
	rl.AddRange(20, 20)
	rl.Print()
	rl.AddRange(20, 21)
	rl.Print()
	rl.AddRange(2, 4)
	rl.Print()
	rl.AddRange(3, 8)
	rl.Print()
	rl.RemoveRange(10, 10)
	rl.Print()
	rl.RemoveRange(10, 11)
	rl.Print()
}