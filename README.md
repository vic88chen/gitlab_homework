# golang implemented range list
**数据结构**
- 数组：有序存储区间的左端点
- map：存储区间的右端点

维护一个有序数组,每次添加移除区间(a1,b1)分别使用二分查找快速找到两个区间端点之前最大的小的左区间点(a2,a3),比较右端点,区间取值:left:min(b2,a1),right:max(b1,b3)
